
const express = require('express');
const path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');

const mongoose = require('mongoose');
const config = require('./config/DB');
const port = 3000;

const itemRoutes = require('./expressRoutes/itemRoutes');
mongoose.Promise = global.Promise;
mongoose.connect(config.DB,{ useNewUrlParser: true ,useUnifiedTopology: true}).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database'+ err)}
);

const app = express();

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cors());



app.use('/items', itemRoutes);

app.get('/', function(req,res){
    res.send('Express Server started.. ');
});
app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))