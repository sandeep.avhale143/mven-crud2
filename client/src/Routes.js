import Create from "./components/Create.vue"
import Edit from "./components/Edit.vue"
import Index from "./components/Index.vue"

export default[
    {path:"/",component:Index},
    {path:"/Index",component:Index},
    {path:"/create-post",component:Create},
    {path:"/edit-post/:id",component:Edit}
]

